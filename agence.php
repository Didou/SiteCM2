<?php
require('config/config.php');
if(!session_id()){ session_start(); };
if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
    $isConnected = true;
    if($user['is_admin'] = 1){
        $isAdmin = true;
    }
} else {
    $isConnected = false;
    $isAdmin = false;
}
?>
<!DOCTYPE html>
<html lang="fr">
<?php include('partials/head.php');?>
<body>

<?php include('partials/header.php');?>

<div class="main">
    <div class="bg-cover" id="bg-cover"></div>

    <div class="content">
        <div id="agences">
            <div class="container">
                <p id="titreAgences">les agences</p>
                <div id="tabAgence">
                    <div id="tabsAgences">
                        <ul>
                            <li id="tabAgence_1"><img src="img/agence/Figurine.png" alt="Figurine"></li>
                            <li id="tabAgence_2"><img src="img/agence/Apikube.png" alt="Apikube"></li>
                            <li id="tabAgence_3"><img src="img/agence/mana.PNG" alt="Mana"></li>
                            <li id="tabAgence_4"><img src="img/agence/Ludixel_logo.png" alt="Ludixel"></li>
                            <li id="tabAgence_5"><img src="img/agence/Logo_SuperFlat.png" alt="Superflat"></li>
                            <li id="tabAgence_6"><img src="img/agence/B_C.png" alt="B&C"></li>
                            <li id="tabAgence_7"><img src="img/agence/Junior_lab.jpg" alt="Junior Lab"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="contenuAgences">
            <div class="container">
                <div id="tabscontent">
                    <div class="tabagence" id="tabagence_1">
                        <div class="part1Agence">
                            <div>
                                <img src="img/agence/Figurine.png" alt="Figurine">
                            </div>
                        </div>
                        <div class="part2Agence">
                            <div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto molestias repellat recusandae laborum nostrum minus atque ipsum quod eligendi sed quisquam, aspernatur harum deleniti, itaque ducimus molestiae, expedita, beatae perspiciatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea ratione quasi beatae ut explicabo possimus, esse aut. Animi quod ab minima beatae eos, possimus doloremque laboriosam, reprehenderit voluptas quaerat eaque!</p>
                            </div>

                        </div>
                    </div>
                    <div class="tabagence" id="tabagence_2">
                        <div class="part1Agence">
                            <div>
                                <img src="img/agence/Apikube.png" alt="Apikube">
                            </div>
                        </div>
                        <div class="part2Agence">
                            <div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto molestias repellat recusandae laborum nostrum minus atque ipsum quod eligendi sed quisquam, aspernatur harum deleniti, itaque ducimus molestiae, expedita, beatae perspiciatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea ratione quasi beatae ut explicabo possimus, esse aut. Animi quod ab minima beatae eos, possimus doloremque laboriosam, reprehenderit voluptas quaerat eaque!</p>
                            </div>

                        </div>
                    </div>
                    <div class="tabagence" id="tabagence_3">
                        <div class="part1Agence">
                            <div>
                                <img src="img/agence/mana.PNG" alt="Mana">
                            </div>
                        </div>
                        <div class="part2Agence">
                            <div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto molestias repellat recusandae laborum nostrum minus atque ipsum quod eligendi sed quisquam, aspernatur harum deleniti, itaque ducimus molestiae, expedita, beatae perspiciatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea ratione quasi beatae ut explicabo possimus, esse aut. Animi quod ab minima beatae eos, possimus doloremque laboriosam, reprehenderit voluptas quaerat eaque!</p>
                            </div>

                        </div>
                    </div>

                    <div class="tabagence" id="tabagence_4">
                        <div class="part1Agence">
                            <div>
                                <img src="img/agence/Ludixel_logo.png" alt="Ludixel">
                            </div>
                        </div>
                        <div class="part2Agence">
                            <div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto molestias repellat recusandae laborum nostrum minus atque ipsum quod eligendi sed quisquam, aspernatur harum deleniti, itaque ducimus molestiae, expedita, beatae perspiciatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea ratione quasi beatae ut explicabo possimus, esse aut. Animi quod ab minima beatae eos, possimus doloremque laboriosam, reprehenderit voluptas quaerat eaque!</p>
                            </div>

                        </div>
                    </div>
                    <div class="tabagence" id="tabagence_5">
                        <div class="part1Agence">
                            <div>
                                <img src="img/agence/Logo_SuperFlat.png" alt="Superflat">
                            </div>
                        </div>
                        <div class="part2Agence">
                            <div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto molestias repellat recusandae laborum nostrum minus atque ipsum quod eligendi sed quisquam, aspernatur harum deleniti, itaque ducimus molestiae, expedita, beatae perspiciatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea ratione quasi beatae ut explicabo possimus, esse aut. Animi quod ab minima beatae eos, possimus doloremque laboriosam, reprehenderit voluptas quaerat eaque!</p>
                            </div>

                        </div>
                    </div>
                    <div class="tabagence" id="tabagence_6">
                        <div class="part1Agence">
                            <div>
                                <img src="img/agence/B_C.png" alt="B&C">
                            </div>
                        </div>
                        <div class="part2Agence">
                            <div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto molestias repellat recusandae laborum nostrum minus atque ipsum quod eligendi sed quisquam, aspernatur harum deleniti, itaque ducimus molestiae, expedita, beatae perspiciatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea ratione quasi beatae ut explicabo possimus, esse aut. Animi quod ab minima beatae eos, possimus doloremque laboriosam, reprehenderit voluptas quaerat eaque!</p>
                            </div>

                        </div>
                    </div>
                    <div class="tabagence" id="tabagence_7">
                        <div class="part1Agence">
                            <div>
                                <img src="img/agence/Junior_lab.jpg" alt="Junior Lab">
                            </div>
                        </div>
                        <div class="part2Agence">
                            <div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto molestias repellat recusandae laborum nostrum minus atque ipsum quod eligendi sed quisquam, aspernatur harum deleniti, itaque ducimus molestiae, expedita, beatae perspiciatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea ratione quasi beatae ut explicabo possimus, esse aut. Animi quod ab minima beatae eos, possimus doloremque laboriosam, reprehenderit voluptas quaerat eaque!</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include('partials/footer.php');?>
</div>


</body>
</html>