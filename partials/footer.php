<div class="footer">
	<div class="container">
		<!-- HTML -->
		<div class="item">
			<div class="sub-item contact">
				<a href="#"></a>
			</div>
		</div>
		<div class="item">
			<div class="sub-item geolocalisation">
				<a href="#">IUT Marne-la-Vallée<br/>2 rue Albert Einstein<br/>77428, Champs-sur-Marne</a>
			</div>
		</div>
		<div class="item">
			<div class="sub-item RS">
				<a href="#" class="facebook"></a>
				<a href="#" class="twitter"></a>
			</div>
		</div>
		<div class="item">
			<div class="sub-item logo">
				<a href="#"></a>
			</div>
		</div>
	</div>
</div>