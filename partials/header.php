<div class="site-content">
	<div class="overlay" id="overlay"></div>
	<div class="site-pusher" id="site-pusher">
		<header class="header">
			<div class="container header-wrapper">

				<div class="icon-resp" id="icon-resp"><span></span></div>

				<div class="logo">
					<a href="index.php">
						<img src="img/logo.png" alt="" class="position">
					</a>
				</div>

				<div class="nuage">
					<div class="nuage-left">
						<a href="#" class="contact" id="btn-contact"></a>
					</div>
					<div class="nuage-right">
						<a href="#" class="faq"></a>
					</div>
				</div>

				<div class="connexion">
					<div class="soucoupe">
						<a href="#" class="log" id="btn-log">Connexion</a>
					</div>
				</div>

				<div class="menu">
					<ul id="nav">
						<li class="item">
							<a href="digivoyage.php">Présentation</a>
							<ul class="sub-menu presentation">
								<li><a href="digivoyage.php">Digivoyage</a></li>
								<li><a href="planning.php">Planning</a></li>
								<li><a href="quisommesnous.php">Qui sommes-nous</a></li>
							</ul>
						</li>
						<li class="item"><a href="agence.php">Les agences</a></li>
						<li class="item"><a href="#">Les workshops</a></li>
						<?php if($isConnected || $isAdmin): ?>
						<li class="item">
							<a href="#">Galerie</a>
							<ul class="sub-menu galerie">
								<li><a href="#">11</a></li>
								<li><a href="#">11</a></li>
							</ul>
						</li>
						<?php endif; ?>
						<li class="item">
							<a href="blog.php">Blog</a>
							<?php if($isAdmin): ?>
							<ul class="sub-menu blog">
								<li><a href="#">Ajout d'articles</a></li>
								<li><a href="#">Edition d'articles</a></li>
							</ul>
							<?php endif; ?>
						</li>
					</ul>
				</div>
			</div>


			<div class="login-wrapper" id="login-wrapper">
				<form action="#" method="post" id="formLogin">
					<div class="input-field">
						<label for="ipt-name">Pseudo :</label>
						<input type="text" name="input[name]" class="ipt-name">
					</div>

					<div class="input-field">
						<label for="ipt-password">Mot de passe :</label>
						<input type="password" name="input[password]" class="ipt-password">
					</div>

					<div class="input-field">
						<input type="submit" value="Envoyer">
					</div>
				</form>
			</div>

			<div class="contact-wrapper" id="contact-wrapper">
				<form action="#" method="post">
					<div class="input-field">
						<label for="ipt-name">Pseudo :</label>
						<input type="text" name="input[name]" class="ipt-name">
					</div>

					<div class="input-field">
						<label for="ipt-mail">Adresse e-mail :</label>
						<input type="email" name="input[email]" class="ipt-email">
					</div>

					<div class="input-field">
						<label for="ipt-object">Objet de votre message :</label>
						<select name="input[objet]" id="select">
							<option value="1">Test 1</option>
							<option value="2">Test 2</option>
							<option value="3">Test 3</option>
							<option value="4">Test 4</option>
						</select>
					</div>

					<div class="input-field">
						<textarea name="input[corps]" id="corps" cols="30" rows="10"></textarea>
					</div>
					<div class="input-field">
						<input type="submit" value="Envoyer">
					</div>
				</form>
			</div>
		</header>