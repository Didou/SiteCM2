<?php
require('config/config.php');
if(!session_id()){ session_start(); };
if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
    $isConnected = true;
    if($user['is_admin'] = 1){
        $isAdmin = true;
    }
} else {
    $isConnected = false;
    $isAdmin = false;
}
?>
<!DOCTYPE html>
<html lang="fr">
<?php include('partials/head.php');?>
<body>

<?php include('partials/header.php');?>

<div class="main">
    <div class="bg-cover" id="bg-cover"></div>

    <div class="content">
        <div class="content-accueil">
            <div class="qui-somme-nous">
                <div class="container">
                    <h1>Qui sommes-nous ?</h1>

                    <div class="left">
                    </div>

                    <div class="right">
                    </div>

                    <div class="cadre">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    <?php include('partials/footer.php');?>
</div>


</body>
</html>