<?php

/**
 * Configuration
 *
 * @author : didier.youn@gmail.com
 * @projet : Digivoyage
 * @collaborators : alice.trinh@gmail.com, maimouna.kone@gmail.com, elina.ollier@gmail.com
 *
 */

try{
    // On récupère la session courante
    session_start();

    // On prépare l'objet PDO pour les requêtes sur la base de données
    $bdd = new PDO('mysql:host=local.dev;dbname=digivoyage','root','');
}
catch (Exception $e){
    die ("Erreur : " . $e->getMessage());
}

// On retourne l'objet PDO
return $bdd;
