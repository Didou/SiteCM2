<?php
require('config/config.php');
if(!session_id()){ session_start(); };
if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
    $isConnected = true;
    if($user['is_admin'] = 1){
        $isAdmin = true;
    }
} else {
    $isConnected = false;
    $isAdmin = false;
}
?>
<!DOCTYPE html>
<html lang="fr">
<?php include('partials/head.php');?>
<body>

<?php include('partials/header.php');?>

<div class="main">
    <div class="bg-cover" id="bg-cover"></div>

    <div class="content">
        <div class="planning">
            <div class="container">

                <h1>Le planning</h1> </br>
                <div id="MainMenu">
                    <div class="list-group panel">

                        <a href="#demo1" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">
                            <ul>
                                <li class="float-l"><i class="fa fa-clock-o"></i> 9h</li>
                                <li>Accueil</li>
                                <li class="float-r"><i class="fa fa-arrow-circle-right"></i></li>
                            </ul>
                        </a>

                        <div class="collapse" id="demo1">
                            <p> <p>Curabitur mollis nunc tincidunt imperdiet posuere. Curabitur gravida auctor ex ac malesuada. Integer consectetur aliquet neque, at placerat est fermentum sed. Praesent mattis justo tellus, ut venenatis orci tincidunt nec. Proin pretium aliquam mauris sit amet consequat. Duis lorem purus, viverra nec finibus non, suscipit nec neque. Donec euismod eget ex at sagittis. In vestibulum commodo bibendum. Etiam ullamcorper urna vitae mi fringilla posuere. Etiam porttitor pellentesque ante a euismod.</p></p>
                        </div>

                        <a href="#demo2" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">
                            <ul>
                                <li class="float-l"><i class="fa fa-clock-o"></i> 9h30</li>
                                <li>Workshop 1</li>
                                <li class="float-r"><i class="fa fa-arrow-circle-right"></i></li>
                            </ul>
                        </a>
                        <div class="collapse" id="demo2">
                            <p>Curabitur mollis nunc tincidunt imperdiet posuere. Curabitur gravida auctor ex ac malesuada. Integer consectetur aliquet neque, at placerat est fermentum sed. Praesent mattis justo tellus, ut venenatis orci tincidunt nec. Proin pretium aliquam mauris sit amet consequat. Duis lorem purus, viverra nec finibus non, suscipit nec neque. Donec euismod eget ex at sagittis. In vestibulum commodo bibendum. Etiam ullamcorper urna vitae mi fringilla posuere. Etiam porttitor pellentesque ante a euismod.</p>
                        </div>

                        <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">
                            <ul>
                                <li class="float-l"><i class="fa fa-clock-o"></i> 10h15</li>
                                <li>Workshop 2</li>
                                <li class="float-r"><i class="fa fa-arrow-circle-right"></i></li>
                            </ul>
                        </a>
                        <div class="collapse" id="demo3">
                            <p>Curabitur mollis nunc tincidunt imperdiet posuere. Curabitur gravida auctor ex ac malesuada. Integer consectetur aliquet neque, at placerat est fermentum sed. Praesent mattis justo tellus, ut venenatis orci tincidunt nec. Proin pretium aliquam mauris sit amet consequat. Duis lorem purus, viverra nec finibus non, suscipit nec neque. Donec euismod eget ex at sagittis. In vestibulum commodo bibendum. Etiam ullamcorper urna vitae mi fringilla posuere. Etiam porttitor pellentesque ante a euismod.</p>
                        </div>

                        <a href="#demo4" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">
                            <ul>
                                <li class="float-l"><i class="fa fa-clock-o"></i> 11h</li>
                                <li>Workshop 3</li>
                                <li class="float-r"><i class="fa fa-arrow-circle-right"></i></li>
                            </ul>
                        </a>
                        <div class="collapse" id="demo4">
                            <p>Curabitur mollis nunc tincidunt imperdiet posuere. Curabitur gravida auctor ex ac malesuada. Integer consectetur aliquet neque, at placerat est fermentum sed. Praesent mattis justo tellus, ut venenatis orci tincidunt nec. Proin pretium aliquam mauris sit amet consequat. Duis lorem purus, viverra nec finibus non, suscipit nec neque. Donec euismod eget ex at sagittis. In vestibulum commodo bibendum. Etiam ullamcorper urna vitae mi fringilla posuere. Etiam porttitor pellentesque ante a euismod.</p>
                        </div>

                        <a href="#demo5" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">
                            <ul>
                                <li class="float-l"><i class="fa fa-clock-o"></i> 11h45</li>
                                <li>Déjeuner</li>
                                <li class="float-r"><i class="fa fa-arrow-circle-right"></i></li>
                            </ul>
                        </a>
                        <div class="collapse" id="demo5">
                            <p>Curabitur mollis nunc tincidunt imperdiet posuere. Curabitur gravida auctor ex ac malesuada. Integer consectetur aliquet neque, at placerat est fermentum sed. Praesent mattis justo tellus, ut venenatis orci tincidunt nec. Proin pretium aliquam mauris sit amet consequat. Duis lorem purus, viverra nec finibus non, suscipit nec neque. Donec euismod eget ex at sagittis. In vestibulum commodo bibendum. Etiam ullamcorper urna vitae mi fringilla posuere. Etiam porttitor pellentesque ante a euismod.</p>
                        </div>

                        <a href="#demo6" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">
                            <ul>
                                <li class="float-l"><i class="fa fa-clock-o"></i> 13h</li>
                                <li>Workshop 4</li>
                                <li class="float-r"><i class="fa fa-arrow-circle-right"></i></li>
                            </ul>
                        </a>
                        <div class="collapse" id="demo6">
                            <p>Curabitur mollis nunc tincidunt imperdiet posuere. Curabitur gravida auctor ex ac malesuada. Integer consectetur aliquet neque, at placerat est fermentum sed. Praesent mattis justo tellus, ut venenatis orci tincidunt nec. Proin pretium aliquam mauris sit amet consequat. Duis lorem purus, viverra nec finibus non, suscipit nec neque. Donec euismod eget ex at sagittis. In vestibulum commodo bibendum. Etiam ullamcorper urna vitae mi fringilla posuere. Etiam porttitor pellentesque ante a euismod.</p>
                        </div>

                        <a href="#demo7" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">
                            <ul>
                                <li class="float-l"><i class="fa fa-clock-o"></i> 13h45</li>
                                <li>Workshop 5</li>
                                <li class="float-r"><i class="fa fa-arrow-circle-right"></i></li>
                            </ul>
                        </a>
                        <div class="collapse" id="demo7">
                            <p>Curabitur mollis nunc tincidunt imperdiet posuere. Curabitur gravida auctor ex ac malesuada. Integer consectetur aliquet neque, at placerat est fermentum sed. Praesent mattis justo tellus, ut venenatis orci tincidunt nec. Proin pretium aliquam mauris sit amet consequat. Duis lorem purus, viverra nec finibus non, suscipit nec neque. Donec euismod eget ex at sagittis. In vestibulum commodo bibendum. Etiam ullamcorper urna vitae mi fringilla posuere. Etiam porttitor pellentesque ante a euismod.</p>
                        </div>

                        <a href="#demo8" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">
                            <ul>
                                <li class="float-l"><i class="fa fa-clock-o"></i> 14h30</li>
                                <li>Workshop 6</li>
                                <li class="float-r"><i class="fa fa-arrow-circle-right"></i></li>
                            </ul>
                        </a>
                        <div class="collapse" id="demo8">
                            <p>Curabitur mollis nunc tincidunt imperdiet posuere. Curabitur gravida auctor ex ac malesuada. Integer consectetur aliquet neque, at placerat est fermentum sed. Praesent mattis justo tellus, ut venenatis orci tincidunt nec. Proin pretium aliquam mauris sit amet consequat. Duis lorem purus, viverra nec finibus non, suscipit nec neque. Donec euismod eget ex at sagittis. In vestibulum commodo bibendum. Etiam ullamcorper urna vitae mi fringilla posuere. Etiam porttitor pellentesque ante a euismod.</p>
                        </div>

                        <a href="#demo9" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">
                            <ul>
                                <li class="float-l"><i class="fa fa-clock-o"></i> 15h15</li>
                                <li>Workshop 7</li>
                                <li class="float-r"><i class="fa fa-arrow-circle-right"></i></li>
                            </ul>
                        </a>
                        <div class="collapse" id="demo9">
                            <p>Curabitur mollis nunc tincidunt imperdiet posuere. Curabitur gravida auctor ex ac malesuada. Integer consectetur aliquet neque, at placerat est fermentum sed. Praesent mattis justo tellus, ut venenatis orci tincidunt nec. Proin pretium aliquam mauris sit amet consequat. Duis lorem purus, viverra nec finibus non, suscipit nec neque. Donec euismod eget ex at sagittis. In vestibulum commodo bibendum. Etiam ullamcorper urna vitae mi fringilla posuere. Etiam porttitor pellentesque ante a euismod.</p>
                        </div>

                        <a href="#demo10" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">
                            <ul>
                                <li class="float-l"><i class="fa fa-clock-o"></i> 16h</li>
                                <li>Fin</li>
                                <li class="float-r"><i class="fa fa-arrow-circle-right"></i></li>
                            </ul>
                        </a>
                        <div class="collapse" id="demo10">
                            <p>Curabitur mollis nunc tincidunt imperdiet posuere. Curabitur gravida auctor ex ac malesuada. Integer consectetur aliquet neque, at placerat est fermentum sed. Praesent mattis justo tellus, ut venenatis orci tincidunt nec. Proin pretium aliquam mauris sit amet consequat. Duis lorem purus, viverra nec finibus non, suscipit nec neque. Donec euismod eget ex at sagittis. In vestibulum commodo bibendum. Etiam ullamcorper urna vitae mi fringilla posuere. Etiam porttitor pellentesque ante a euismod.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php include('partials/footer.php');?>
</div>


</body>
</html>