<?php

/**
 * Login
 *
 * @author : didier.youn@gmail.com
 * @projet : Digivoyage
 * @collaborators : alice.trinh@gmail.com, maimouna.kone@gmail.com, elina.ollier@gmail.com
 *
 */

// On récupère la session et l'objet PDO
require('../config/config.php');

/**
 * Get POST params
 */
$username = htmlspecialchars($_POST['name']);
$password = htmlspecialchars($_POST['password']);

if(isset($username) && isset($password)){
    $req = $bdd->prepare('SELECT * FROM users WHERE name LIKE :name AND password LIKE :password ');
    $req->execute(array(':name' => '%'.$username.'%', ':password' => '%'.$password.'%'));
    $user = $req->fetch();
    if($user){
        $_SESSION['user'] = $user;
        echo json_encode(1);
        return true;
    }
}
echo json_encode(-1);
return false;