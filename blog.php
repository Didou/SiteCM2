<?php
require('config/config.php');
if(!session_id()){ session_start(); };
if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
    $isConnected = true;
    if($user['is_admin'] = 1){
        $isAdmin = true;
    }
} else {
    $isConnected = false;
    $isAdmin = false;
}
?>
<!DOCTYPE html>
<html lang="fr">
<?php include('partials/head.php');?>
<body>

<?php include('partials/header.php');?>

<div class="main">
    <div class="bg-cover" id="bg-cover"></div>

    <div class="content">
        <div class="blog">
            <div class="container">
                <h1>Le Blog</h1>

                <div class="entête">
                    <h2>17/04/2016 - Une journée parfaite en perspective</h2>
                </div>

                <div class="cadre">
                    <img src="img/enfants.jpg" alt="">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur.

                        Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur.
                        </br>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur.

                        Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur.
                    </p>
                </div>
                </br>
                <div class="more-article">
                    <div class="more-article1">

                    </div>

                    <div class="more-article2">

                    </div>

                    <div class="more-article3">
                    </div>
                </div>
                <!--! CONTENU : VOTRE CODE HTML ! -->
            </div>
        </div>
    </div>

    <?php include('partials/footer.php');?>
</div>


</body>
</html>