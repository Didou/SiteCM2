<?php
require('config/config.php');
if(!session_id()){ session_start(); };
if(isset($_SESSION['user'])){
	$user = $_SESSION['user'];
	$isConnected = true;
	if($user['is_admin'] = 1){
		$isAdmin = true;
	}
} else {
	$isConnected = false;
	$isAdmin = false;
}
?>
<!DOCTYPE html>
<html lang="fr">
<?php include('partials/head.php');?>
<body>

	<?php include('partials/header.php');?>

	<div class="main">
		<div class="bg-cover" id="bg-cover"></div>

		<div class="content">
			<div class="days-left-wrapper">
				<div class="container">
					<div class="day-left">
						<span>J-</span>
						<span id="day-left"></span>
					</div>
				</div>
			</div>
			<div class="agence-wrapper">
				<div class="container">
					<div class="wrapper" id="agence-wrapper">
						<h1>Les agences</h1>
						<div class="agences">
							<button id="next" class="btn_agence btn_next"></button>
							<button id="prev" class="btn_agence btn_prev"></button>
							<div class="overflow">
								<div class="agence next" data="index-1">
									<div class="logo">
										<div class="bg_logo"></div>
										<img src="img/agence/figurine.png" alt="">
									</div>
								</div>
								<div class="agence visible" data="index-2">
									<div class="logo">
										<div class="bg_logo"></div>
										<img src="img/agence/Apikube.png" alt="">
									</div>
								</div>
								<div class="agence next" data="index-3">
									<div class="logo">
										<div class="bg_logo"></div>
										<img src="img/agence/B_C.png" alt="">
									</div>
								</div>
								<div class="agence next" data="index-4">
									<div class="logo">
										<div class="bg_logo"></div>
										<img src="img/agence/Junior_lab.jpg" alt="">
									</div>
								</div>
								<div class="agence next" data="index-5">
									<div class="logo">
										<div class="bg_logo"></div>
										<img src="img/agence/Logo_SuperFlat.png" alt="">
									</div>
								</div>
								<div class="agence next" data="index-6">
									<div class="logo">
										<div class="bg_logo"></div>
										<img src="img/agence/Ludixel_logo.png" alt="">
									</div>
								</div>
								<div class="agence next" data="index-7">
									<div class="logo">
										<div class="bg_logo"></div>
										<img src="img/agence/mana.PNG" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include('partials/footer.php');?>
	</div>


</body>
</html>