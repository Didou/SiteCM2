$(document).ready(function () {

    var $overlay = $('.overlay');

    // Navigation Responsive
    var $nav 			= $('#nav'),
        $btnResponsive 	= $('#icon-resp'),
        $sitePusher 	= $('#site-pusher'),
        $bgShadow		= $('#bg-cover');

    // Sous-menu
    var $item 			= $nav.find('li'),
        $subMenu		= $item.find('.sub-menu');

    // Login
    var $btnLogin 		= $('#btn-log'),
        $formLogin      = $('#formLogin'),
        $loginWrapper 	= $('#login-wrapper');

    // Contact
    var $btnContact 	= $('#btn-contact'),
        $contactWrapper = $('#contact-wrapper');

    // Index - Jour
    var DATE_FINAL		= '2016/05/21';

    // Index - Agence
    var agences 		= $('div.agence');
    var current_agence  = $('div.agence.current');

    // Compteur date
    initDayLeft(DATE_FINAL, 250);
    // Slider
    initAgenceSlider(agences);


    $btnResponsive.on('click', function (e) {
        var $this = $(this);

        if($this.hasClass('is-active')){
            $this.removeClass('is-active');
            $sitePusher.removeClass('nav-on');
            $nav.removeClass('on-responsive');
            $bgShadow.removeClass('is-active');
        } else {
            $this.addClass('is-active');
            $sitePusher.addClass('nav-on');
            $nav.addClass('on-responsive');
            $bgShadow.addClass('is-active');
        }
    });

    $item.on({
        mouseenter : function (e) {
            var $this = $(this);
            var parent = $this.parent();
            var $sub  = $this.find('ul');

            if(!parent.hasClass('on-responsive')){
                $this.addClass('on-hover');
                $sub.slideDown().addClass('sub-active');
            }
        },

        mouseleave : function (e) {
            var $this = $(this);
            var parent = $this.parent();
            var $sub  = $this.find('ul');

            if($sub.hasClass('sub-active')){
                $sub.slideUp();
            }
        }
    });

    $subMenu.on({
        mouseleave : function (e) {
            var $this = $(this);
            var parent = $this.parent();

            if(parent.has('on-hover')){
                if($this.hasClass('sub-active')){
                    $this.slideUp().removeClass('sub-active');
                    parent.removeClass('on-hover');
                }
            }

        }
    });

    // Listener overlay
    $overlay.on('click', function(e){
        focusOutOverlay($overlay, $loginWrapper, $contactWrapper);
    });

    // Bouton Login
    $btnLogin.on('click', function (e) {
        $loginWrapper.addClass('is-active');
        $overlay.addClass('is-active');
        setTimeout(function(){
            $loginWrapper.css('animation', 'none');
        }, 4000);
    });

    $formLogin.on('submit', function (e) {
        e.preventDefault();

        var $name       = $(this).find('input[name="input[name]"]').val(),
            $password   = $(this).find('input[name="input[password]"]').val();

        $.ajax({
            url         : '/PROJET/SITECM2/ajax/login.php',
            method      : 'POST',
            dataType    : 'json',
            data        : {
                name        : $name,
                password    : $password
            },
            success     : function(response){
                if(parseInt(response) == 1){
                    focusOutOverlay($overlay, $loginWrapper, $contactWrapper);
                }
            }
        });
    });

    // Bouton de contact
    $btnContact.on('click', function(e) {
        $contactWrapper.addClass('is-active');
        $overlay.addClass('is-active');
    });
});

function focusOutOverlay ($overlay, $loginWrapper, $contactWrapper) {
    if($overlay.hasClass('is-active')){
        $overlay.removeClass('is-active');
    }

    if($loginWrapper.hasClass('is-active')){
        $loginWrapper.removeClass('is-active');
        $loginWrapper.css('animation', 'move 3s infinite');
    }

    if($contactWrapper.hasClass('is-active')){
        $contactWrapper.removeClass('is-active');
    }
}

function initDayLeft (final_date, indice) {
    var today 		= new Date(),
        final 		= new Date(final_date),
        timestamp 	= 24*60*60*1000,
        dayLeft 	= Math.round(Math.abs((today.getTime() - final.getTime()) / timestamp));

    var timing		= 30;

    var div 		= $('#day-left');
    var index 		= indice;

    var timer = setInterval(function () {
        if (parseInt(index) <= dayLeft) {
            clearInterval(timer);
            return true;
        }
        index--;
        div.html(index);
    }, timing);
}

function initAgenceSlider(agences) {
    var wrapper 		= $('#agence-wrapper'),
        wrapperWidth	= wrapper.width(),
        width			= Math.round(wrapperWidth / 3);

    var overflow_POS 	= wrapper.find('.overflow');

    agences.each(function(){
        $(this).css('width', width + 'px');
    });

    var $btn = $('.btn_agence');
    var posX = parseInt(overflow_POS.css('left'));

    $('.btn_prev').css('left', width + 'px');
    $('.btn_next').css('right', width + 'px');

    var posMin 	= width;
    var posMax 	= getPositionMax(agences, width);

    $btn.on('click', function (e) {
        var $this 	= $(this);

        var agences = $('div.agence');
        var current = $('div.visible');

        if($this.hasClass('btn_next')){
            if(posX < (posMax * -1) + (width * 3)){
                return true;
            } else {
                posX = posX - width;

                var $next = current.next();

                $next.removeClass('next').addClass('visible');
                current.removeClass('visible').addClass('next');
            }
        }

        if($this.hasClass('btn_prev')){
            if(posX > 0) {
                return true;
            }
            posX = posX + width;

            var $prev = current.prev();

            $prev.removeClass('next').addClass('visible');
            current.removeClass('visible').addClass('next');
        }

        overflow_POS.animate({
            left : posX + 'px'
        }, 750);
    });
}

function getPositionMax(agences, width) {
    var posMax = 0;

    for(var i = 0 ; i < agences.length ; i++) {
        posMax = posMax + parseInt(width);
    }

    return posMax;
}