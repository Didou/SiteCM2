window.onload=function() {
    var fondAgence = document.getElementById("contenuAgences");
    fondAgence.style.backgroundImage = "url('.../../img/fondsAgences/pixel.png')";

    // get tab container
    var container = document.getElementById("tabAgence");
    var tabcon = document.getElementById("tabscontent");
    //alert(tabcon.childNodes.item(1));
    // set current tab
    var navitem = document.getElementById("tabAgence_1");

    //store which tab we are on
    var ident = navitem.id.split("_")[1];
    //alert(ident);
    navitem.parentNode.setAttribute("data-current",ident);
    //set current tab with class of activetabheader
    navitem.setAttribute("class","tabActiveHeader");

    //hide two tab contents we don't need
    var pages = tabcon.getElementsByClassName("tabagence");
    for (var i = 1; i < pages.length; i++) {
        pages.item(i).style.display="none";
    };

    //this adds click event to tabs
    var tabs = container.getElementsByTagName("li");
    for (var i = 0; i < tabs.length; i++) {
        tabs[i].onclick=displayPage;
    }
}

// on click of one of tabs
function displayPage() {
    var current = this.parentNode.getAttribute("data-current");
    //remove class of activetabheader and hide old contents
    document.getElementById("tabAgence_" + current).removeAttribute("class");
    document.getElementById("tabagence_" + current).style.display="none";

    var ident = this.id.split("_")[1];
    //add class of activetabheader to new active tab and show contents
    this.setAttribute("class","tabActiveHeader");
    document.getElementById("tabagence_" + ident).style.display="block";
    this.parentNode.setAttribute("data-current",ident);

    var fondAgence = document.getElementById("contenuAgences");

    var idAgence=this.id;

    switch (idAgence){
        case 'tabAgence_1': fondAgence.style.backgroundImage = "url(.../../img/fondsAgences/pixel.png)";
            break;

        case 'tabAgence_2': fondAgence.style.backgroundImage = "url(.../../img/fondsAgences/conquete.png)";
            break;

        case 'tabAgence_3': fondAgence.style.backgroundImage = "url(.../../img/fondsAgences/egypte.png)";
            break;

        case 'tabAgence_4': fondAgence.style.backgroundImage = "url(.../../img/fondsAgences/1789.png)";
            break;

        case 'tabAgence_5': fondAgence.style.backgroundImage = "url(.../../img/fondsAgences/cinema.png)";
            break;

        case 'tabAgence_6': fondAgence.style.backgroundImage = "url(.../../img/fondsAgences/renaissance.png)";
            break;

        case 'tabAgence_7': fondAgence.style.backgroundImage = "url(.../../img/fondsAgences/romantique.png)";
            break;

        default:  fondAgence.style.backgroundImage = "url(.../../img/fondsAgences/pixel.png)";
    }
}/**
 * Created by DiDiER on 10/04/2016.
 */
